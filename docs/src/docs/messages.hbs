---
component: Messages
analytics:
  pageCategory: component
  component: message
design: https://design.atlassian.com/latest/product/components/messages/
status:
  api: general
  core: true
  wrm: com.atlassian.auiplugin:message
  amd: false
  experimentalSince: 3.0
  generalSince: 5.0
---

<h2>Summary</h2>
<p>Messages are the primary method for providing system feedback in the product user interface. Messages include notifications of various kinds: alerts, confirmations, notices, warnings, info and errors.</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>
<p>There are several message types with different colours and icons.</p>

<article class="aui-flatpack-example messages-example">
    <h3>Inserted with HTML</h3>
    <div class="aui-message">
        <p class="title">
            <strong>Backup stale</strong>
        </p>
        <p>This instance was last backed up on <time>Thursday, 18 September 2011.</time></p>
    </div>

    <div class="aui-message aui-message-warning">
        <p class="title">
            <strong>Backing up attachments</strong>
        </p>
        <p>Attachments will not be backed up. This needs to be done manually.</p>
    </div>

    <div class="aui-message aui-message-error">
        <p class="title">
            <strong>Destructive operation!</strong>
        </p>
        <p>Data import will wipe all existing content - make sure you backup first!</p>
    </div>

    <div class="aui-message aui-message-success">
        <p class="title">
            <strong>Success!</strong>
        </p>
        <p>You have backed up your system to <code>C:/backups/filename.xml.</code></p>
    </div>

    <h3>Inserted with JS</h3>
    <div id="aui-message-bar"></div>
    <div id="custom-context"></div>
    <script>
    AJS.$(document).ready(function() {
        // begin demo javascript

        AJS.messages.success({
            body: "You successfully backed up your system! High-five!"
        });

        AJS.messages.info("#custom-context", {
            title: "Custom Context Demo",
            body: '<p>Because you often need to specify where the message goes.</p>'
        });

        AJS.messages.warning("#custom-context", {
            body: '<p>It\'s dark in here... just the way Grues like it.</p>' +
            '<ul class="aui-nav-actions-list">' +
            '<li><button class="aui-button aui-button-link">Turn on the light</button></li>' +
            '<li><button class="aui-button aui-button-link">Take your chances</button></li>' +
            '</ul>'
        });

        // end demo javascript
    });
    </script>
</article>


<h2>Code</h2>
<p>
    There are a few ways to implement messages: using HTML (or Soy to generate the HTML) or using JavaScript.
</p>

<h3>HTML</h3>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-message aui-message-error">
            <p class="title">
                <strong>Error!</strong>
            </p>
            <p>And this is just content in a Default message.</p>
        </div>
    </noscript>
</aui-docs-example>

<h3>JavaScript</h3>

<p>
    Messages can be added through AUI's JavaScript API in two main ways.
    The most common is to inject the element inside a "context" element:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/js">
        AJS.messages.info("#a-custom-context", {
            title: 'This is a title in a default message.',
            body: '<p> And this is just content in a Default message.</p>'
        });
    </noscript>
    <noscript type="text/html">
        <div id="a-custom-context"></div>
    </noscript>
</aui-docs-example>

<p>
    If the context selector is omitted, the message is added to a static location: an element
    with <code>id="aui-message-bar"</code>.
</p>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/js">
        AJS.messages.info({
           title: 'This is a title in a default message.',
           body: '<p> And this is just content in a Default message.</p>'
        });
    </noscript>
    <noscript type="text/html">
        <div id="aui-message-bar"></div>
    </noscript>
</aui-docs-example>

<p>
    Note that if you do not set the context, and the aui-message-bar element does not exist,
    the message will not appear as it has no target location.
</p>

<h3>Soy</h3>

<noscript is="aui-docs-code" type="text/html">
    {call aui.message.warning}
        {param title: 'An error occurred - user intervention required!' /}
        {param closeable: 'true' /}
        {param id: 'messageIDattribute' /}
        {param content}
            <p>Some details about the error so the user knows what to do.</p>
        {/param}
    {/call}
</noscript>

<h3>Types of messages</h3>
<p>When adding an HTML message, you must ensure root element (.aui-message) has the desired message class. Calling from Soy or JavaScript wraps this into one call, for convenience.</p>
<p>Note: icon class have been deprecated. The old message classes are also deprecated, now namespaced with aui-message-</p>
<table class="aui">
    <thead>
        <tr>
            <th>Message type</th>
            <th>Message class</th>
            <th>Soy call</th>
            <th>JavaScript function</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Generic <span class="aui-lozenge aui-lozenge-error">Deprecated</span></td>
            <td><code>aui-message-generic</code>
            </td>
            <td><code>call aui.message.generic</code>
            </td>
            <td><code>AJS.messages.generic()</code>
            </td>
        </tr>
        <tr>
            <td>Error</td>
            <td><code>aui-message-error</code>
            </td>
            <td><code>call aui.message.error</code>
            </td>
            <td><code>AJS.messages.error()</code>
            </td>
        </tr>
        <tr>
            <td>Warning</td>
            <td><code>aui-message-warning</code>
            </td>
            <td><code>call aui.message.warning</code>
            </td>
            <td><code>AJS.messages.warning()</code>
            </td>
        </tr>
        <tr>
            <td>Success</td>
            <td><code>aui-message-success</code>
            </td>
            <td><code>call aui.message.success</code>
            </td>
            <td><code>AJS.messages.success()</code>
            </td>
        </tr>
        <tr>
            <td>Info</td>
            <td><code>aui-message-info</code>
            </td>
            <td><code>call aui.message.info</code>
            </td>
            <td><code>AJS.messages.info()</code>
            </td>
        </tr>
        <tr>
            <td>Hint <span class="aui-lozenge aui-lozenge-error">Deprecated</span></td>
            <td><code>aui-message-hint</code>
            </td>
            <td><code>call aui.message.hint</code>
            </td>
            <td><code>AJS.messages.hint()</code>
            </td>
        </tr>
    </tbody>
</table>

<h3>Message actions</h3>

<p>
    If the message is more than just informational or there are obvious follow-up things a user could do,
    add a list of actions to the message body to allow the user to easily take their next step.
    <br/>
    Actions should have the appearance of a link, but should use the appropriate HTML element for
    the action &mdash; for example, use a <code>&lt;button&gt;</code> when the action is handled
    by JavaScript or does something on the current page; use an <code>&lt;a&gt;</code> when the action
    can take the user to another location in the system.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-message aui-message-warning">
            <p>It's dark in here... just the way Grues like it.</p>
            <ul class="aui-nav-actions-list">
                <li><button class="aui-button aui-button-link">Turn on the light</button></li>
                <li><button class="aui-button aui-button-link">Take your chances</button></li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>

<h3>Closeable</h3>

<p>If you want to allow the user to dismiss the message</p>

<h2>Options</h2>
<h3>HTML options</h3>
<p>These options are set by adding classes to the root <code>aui-message</code> div.</p>
<table class="aui">
    <thead>
        <tr>
            <th>Class</th>
            <th>Effect</th>
            <th>Example</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>closeable</code>
            </td>
            <td>Adds a Close icon to the message which closes and removes the message when clicked.</td>
            <td>
                <noscript is="aui-docs-code" type="text/html">
                    <div class="aui-message closeable">...</div>
                </noscript>
            </td>
        </tr>
        <tr>
            <td><code>fadeout</code>
            </td>
            <td>Since 5.1. Makes the message fade away after five seconds. The fadeout will be cancelled if the user interacts with it (hover or focus). Note the fadeout option is best used via JavaScript and should not be used on critical errors and other
                information the user must be aware of.</td>
            <td>
                <noscript is="aui-docs-code" type="text/html">
                    <div class="aui-message fadeout">...</div>
                </noscript>
            </td>
        </tr>
    </tbody>
</table>

<h3>Soy options</h3>
<p>These options are set by adding params to the Soy call.</p>
<table class="aui">
    <thead>
        <tr>
            <th>Param</th>
            <th>Effect</th>
            <th>Default</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>content</td>
            <td>Required. Content to display within the message.</td>
            <td>n/a</td>
        </tr>
        <tr>
            <td>titleContent</td>
            <td>Title text of the message.</td>
            <td>n/a</td>
        </tr>
        <tr>
            <td>id</td>
            <td>ID attribute</td>
            <td>n/a</td>
        </tr>
        <tr>
            <td>isCloseable</td>
            <td>Boolean. Set to true, makes the Message closeable.</td>
            <td>false</td>
        </tr>
    </tbody>
</table>

<h3>JavaScript options</h3>
<p>These options are set in the options object when creating a Message with JavaScript:</p>
<table class="aui">
    <thead>
        <tr>
            <th>Option</th>
            <th>Description</th>
            <th>Possible values</th>
            <th>Default</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>(context argument)</td>
            <td>You can override the default context by passing it into the first argument of the messages function. This is the only option set as an argument.</td>
            <td>A string in the form of an ID selector</td>
            <td>#aui-message-bar</td>
        </tr>
        <tr>
            <td><code>body</code>
            </td>
            <td>The main content of the message.</td>
            <td>HTML</td>
            <td>none</td>
        </tr>
        <tr>
            <td><code>closeable</code>
            </td>
            <td>Adds a control allowing the user to close the message, removing it from the page.</td>
            <td>boolean</td>
            <td>true</td>
        </tr>
        <tr>
            <td><code>id</code>
            </td>
            <td>Gives your message an ID attribute, useful for selecting the message later.</td>
            <td>ID string (no hash)</td>
            <td>none</td>
        </tr>
        <tr>
            <td><code>insert</code>
            </td>
            <td>Sets the insert point to the start (prepend) or end (append) of the context element. (Option added in AUI 4.2)</td>
            <td>prepend, append</td>
            <td>append</td>
        </tr>
        <tr>
            <td><code>shadowed</code>
            </td>
            <td>Toggles the dropshadow on the message box. Usually not changed from default.</td>
            <td>boolean</td>
            <td>true</td>
        </tr>
        <tr>
            <td><code>title</code>
            </td>
            <td>Sets the title text of the message.</td>
            <td>Plain text</td>
            <td>none</td>
        </tr>
        <tr>
            <td><code>fadeout</code>
            </td>
            <td>(since 5.1) Toggles the fade away on the message</td>
            <td>boolean</td>
            <td>false</td>
        </tr>
        <tr>
            <td><code>delay</code>
            </td>
            <td>(since 5.1) Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)</td>
            <td>number</td>
            <td>5000</td>
        </tr>
        <tr>
            <td><code>duration</code>
            </td>
            <td>(since 5.1) Fadeout animation duration in milliseconds (ignored if fadeout==false)</td>
            <td>number</td>
            <td>500</td>
        </tr>
    </tbody>
</table>

<h3>AJS.messages events</h3>
<table class="aui">
    <thead>
        <tr>
            <th>Event</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>messageClose</code></td>
            <td><span class="aui-lozenge aui-lozenge-error">Deprecated</span> When a message is closed, messageClose is fired before the message is removed from the DOM, including a reference to the DOM element being removed.</td>
        </tr>
        <tr>
            <td><code>aui-message-close</code></td>
            <td>When a message is closed, aui-message-close is fired AFTER the element is removed, a reference to the message being removed is included in the event data.</td>
        </tr>
    </tbody>
</table>

<noscript is="aui-docs-code" type="text/js">
    $(document).on('aui-message-close', function (e) {
        AJS.log('Message id: ' + e.target.attr('id'));
    });
</noscript>
