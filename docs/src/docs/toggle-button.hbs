---
component: Toggle Button
analytics:
  pageCategory: component
  component: toggle
  componentApiType: web-component
design: https://design.atlassian.com/latest/product/components/buttons#toggle-button
status:
  api: general
  core: false
  wrm: com.atlassian.auiplugin:aui-toggle
  amd: false
  experimentalSince: 5.9
  generalSince: 7.8
  webComponentSince: 5.9
---

<h2>Summary</h2>

<p>A toggle button provides a quick way to see whether a given option is enabled or disabled and to toggle between these states.
    Place a toggle button to the right of a label, in a table, or below a section describing what is enabled or disabled.</p>

<p>A toggle button should be used if the main intent is to make a binary choice,
    such as turning something on or off, and the choice is not part of a form.</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>
<h3>Default toggle</h3>

<aui-docs-example live-demo>
    <noscript type="text/html">
        <form class="aui">
            <div class="field-group">
                <aui-label for="gzip-compression">Use gzip compression</aui-label>
                <aui-toggle id="gzip-compression" label="Use gzip compression"></aui-toggle>
            </div>
            <div class="field-group">
                <aui-label for="outgoing-email">Send outgoing email</aui-label>
                <aui-toggle id="outgoing-email" label="Send outgoing email" checked tooltip-on="Enabled" tooltip-off="Disabled"></aui-toggle>
            </div>
            <div class="field-group">
                <aui-label for="email-visibility">User email visibility</aui-label>
                <aui-toggle id="email-visibility" label="User email visibility" disabled tooltip-on="Public" tooltip-off="Hidden"></aui-toggle>
            </div>
            <div class="field-group">
                <aui-label for="email-confirm">Email confirmation</aui-label>
                <aui-toggle id="email-confirm" label="Email confirmation" checked disabled tooltip-on="Email confirmation is currently enabled. You need to have admin permission to turn it off."></aui-toggle>
            </div>
        </form>
    </noscript>
</aui-docs-example>

<p>Define a basic toggle button.</p>

<noscript is="aui-docs-code" type="text/html">
    <aui-toggle id="gzip-compression" label="use gzip compression"></aui-toggle>
</noscript>

<p>
    The <code>label</code> attribute is required for a screen reader to announce the button. If you want to have an
    actual visible label, use <code>&lt;aui-label&gt;</code>.
</p>

<noscript is="aui-docs-code" type="text/html">
    <aui-label for="gzip-compression">Use gzip compression</aui-label>
</noscript>

<p>
    You can set the default <code>checked</code> and <code>disabled</code> states directly in the HTML, or
    using Javascript to modify the elements' <a href="#attributes">attributes/properties</a>.
</p>

<p>
    Sometimes you need to provide more information relating to the state of a control. This can be achieved using the
    <code>tooltip-on</code> and <code>tooltip-off</code> <a href="#attributes">attributes</a>.
</p>


<h3>Asynchronous toggle</h3>

<p>
    The busy property can be used to handle asynchronous requests. Bind to the <code>change</code> event
    to trigger an AJAX request and display a spinner when the toggle is pressed:
</p>

<aui-docs-example live-demo>
    <noscript type="text/html">
        <form class="aui">
            <div class="field-group">
                <aui-label for="wifi-toggle">WiFi</aui-label>
                <aui-toggle id="wifi-toggle" label="Wifi toggle"></aui-toggle>
            </div>
        </form>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        var toggle = document.getElementById('wifi-toggle');
        toggle.addEventListener('change', function(e) {
            var isChecked = toggle.checked;     // new value of the toggle
            toggle.busy = true;
            $.post('set/my/variable', { value: isChecked })
                .done(function () {
                    console.log('success');
                })
                .fail(function () {
                    toggle.checked = !isChecked;
                    console.error('display an error message');
                })
                .always(function () {
                    toggle.busy = false;
                });
        });
    </noscript><!--/aui-docs-code-->
    <noscript type="text/js">
        // create fake server response
        var url = "set/my/variable";
        var server = sinon.fakeServer.create();
        server.autoRespond = true;
        server.autoRespondAfter = 2000;

        server.respondWith('POST', url, [
            200,
            { 'Content-Type': 'application/json' },
            ''
        ]);
    </noscript>
</aui-docs-example>


<h2>API Reference</h2>
<h3 id="attributes">Attributes and properties</h3>

<p>Attributes and properties can be set and accessed directly on the <code>aui-toggle</code> element.</p>

<noscript is="aui-docs-code" type="text/js">
    var toggle = document.getElementById('my-toggle');
    toggle.checked = true;
    toggle.disabled = true;
</noscript><!--/aui-docs-code-->

<table class="aui">
    <thead>
    <tr>
        <th>Name</th>
        <th>Attribute</th>
        <th>Property</th>
        <th>Type</th>
        <th class="description">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>label</code> <span class="aui-lozenge aui-lozenge-current">required</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>Text used by screen readers when announcing the toggle button (required for accessibility).</p>
        </td>
    </tr>
    <tr>
        <td><code>checked</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>If set, the toggle will be set to the 'on' state.</td>
    </tr>
    <tr>
        <td><code>disabled</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>If set, the user will not be able to interact with the toggle.</td>
    </tr>
    <tr>
        <td><code>form</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-warning">is a read-only property</span></td>
        <td>String</td>
        <td>
            <p>The <code>form</code> element that the toggle button is associated with. The attribute is the id of the <code>form</code> element, while the read-only property returns the element.</p>
            <p><i>HTML5 attribute. Not supported in IE10 or older.</i></p>
        </td>
    </tr>
    <tr>
        <td><code>name</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>The name of the toggle button element, used to reference the element in JavaScript or form data.</td>
    </tr>
    <tr>
        <td><code>value</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>The value associated with the toggle button element (sent on form submission).</td>
    </tr>
    <tr>
        <td><code>busy</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">is not an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>
            <p>Sets the toggle to a busy state, displaying the busy spinner.</p>
        </td>
    </tr>
    <tr>
        <td><code>tooltip-on</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>
                Sets the tooltip text that is shown when the toggle is set to the ‘on’ state.<br>
                The default value is <code>On</code>.
            </p>
            <noscript is="aui-docs-code" type="text/js">
                toggle.setAttribute('tooltip-on', 'Enabled');
                toggle.tooltipOn = 'Enabled';
            </noscript><!--/aui-docs-code-->
        </td>
    </tr>
    <tr>
        <td><code>tooltip-off</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>
                Sets the tooltip text that is shown when the toggle is set to the ‘off’ state.<br>
                The default value is <code>Off</code>.
            </p>
            <noscript is="aui-docs-code" type="text/js">
                toggle.setAttribute('tooltip-off', 'Disabled');
                toggle.tooltipOff = 'Disabled';
            </noscript><!--/aui-docs-code-->
        </td>
    </tr>
    </tbody>
</table>

