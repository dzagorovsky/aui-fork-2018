module.exports = function (verbose) {
    var isLastLogError = false;

    return function(data) {
        data = data.toString().trim();
        if (data.indexOf('[INFO]') === 0) {
            if (verbose) console.log(data);
        } else if (data.indexOf('[WARNING]') === 0) {
            console.warn(data);
            if (isLastLogError) {
                process.exit(1);
            }
        } else if (data.indexOf('[ERROR]') === 0) {
            isLastLogError = true;
            console.error(data);
        } else {
            if (verbose) console.log(data);
        }
    };
};
