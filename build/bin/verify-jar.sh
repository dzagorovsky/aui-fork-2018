#!/bin/bash
set -x
set -e

# Fail fast if we don't have the requisite dependencies
command -v yarn >/dev/null 2>&1 || { echo >&2 "Verifying the JAR requires Yarn but it's not installed. Aborting."; exit 2; }
command -v jar >/dev/null 2>&1 || { echo >&2 "Verifying the JAR requires Java and the JDK but it's not installed. Aborting."; exit 2; }
command -v mvn >/dev/null 2>&1 || { echo >&2 "Verifying the JAR requires Maven but it's not installed. Aborting."; exit 2; }
command -v xmlstarlet >/dev/null 2>&1 || { echo >&2 "Verifying the JAR requires xmlstarlet but it's not installed. Aborting."; exit 2; }

XMLSTARLET=$(command -v xmlstarlet)

# Run wrm task
yarn gulp wrm

AUIADG_DIR=$(pwd)
TMP=$(mktemp -d /tmp/auiplugin.XXXXXXX)
VERSION=$(grep '"version":' package.json | sed -E 's/.*"version": "([^"]+)".*/\1/')

JAR=$1
if [ -z "$1" ]; then
    JAR=${AUIADG_DIR}/integration/plugin/target/auiplugin-${VERSION}.jar
fi

# Build the plugin (if not already present) and fail it we can't build it.
if [ ! -f ${JAR} ]; then
    yarn p2-plugin 1>&2
    if [ ! -f ${JAR} ]; then
      echo Could not build auiplugin-${VERSION}.jar. 1>&2
      exit 1
    fi
fi

mkdir -p ${TMP}/auiplugin-${VERSION}
cd ${TMP}/auiplugin-${VERSION}
jar xf ${JAR}

# Make sure the jar contains atlassian-plugin.xml, otherwise our pipe chain
# below may not return non-zero (and fail the build via set -e) in the case it
# is missing.
PLUGIN_XML=atlassian-plugin.xml
if [ ! -f ${PLUGIN_XML} ]; then
  echo ${PLUGIN_XML} missing from jar. 1>&2
  exit 1
fi

# If any extra module folders are declared, make sure they exist in the jar
# and are non-empty.
PLUGIN_POM="$(dirname ${PLUGIN_XML})/META-INF/maven/com.atlassian.aui/auiplugin/pom.xml"
if [ ! -f ${PLUGIN_POM} ]; then
  echo ${PLUGIN_POM} missing from jar. 1>&2
  exit 1
fi

EXTRA_MODULE_DIRS=$(${XMLSTARLET} sel --text \
                                      --template \
                                      --match '//*[local-name() = "plugin"][*[local-name() = "groupId"]="com.atlassian.maven.plugins"]/*[local-name() = "configuration"]/*[local-name() = "instructions"]/*[local-name() = "Atlassian-Scan-Folders"]' \
                                      --value-of "concat('$(dirname ${PLUGIN_XML})/', node())" \
                                      --output ' ' \
                                      ${PLUGIN_POM})
for dir in ${EXTRA_MODULE_DIRS}; do
  if [ ! "$(ls -A ${dir}/*.xml)" ]; then
    echo "Extra plugin XML modules missing: ${dir}/*.xml" 1>&2
    exit 1
  fi
done

##
# Verify the plugin XML
#
retcode=0
${AUIADG_DIR}/build/bin/verify-plugin-xml.sh ${PLUGIN_XML} ${PLUGIN_POM} || retcode=$?

EXTRA_MODULES=$(find ${EXTRA_MODULE_DIRS} -type f -name '*.xml')
XML_FILES="${PLUGIN_XML} ${EXTRA_MODULES}"

##
# Ensure all downloadable resources actually exist in the jar.
#
mkfifo resource_paths
${XMLSTARLET} sel --template \
                  --match '//web-resource/resource[@type="download"]' \
                  --value-of '@location' --nl \
                  ${XML_FILES} \
    | sort --unique \
    > resource_paths &

while read path; do
    if [[ ${path} == */ ]]; then
        if [ ! -d ${path} ]; then
            retcode=1
            echo Missing directory: ${path}
        fi
    else
        if [ ! -f ${path} ]; then
            retcode=1
            echo Missing file: ${path}
        fi
    fi
done < resource_paths

cd ${AUIADG_DIR}
rm -rf ${TMP}
exit ${retcode}
