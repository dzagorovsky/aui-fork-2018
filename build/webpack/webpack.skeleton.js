const path = require('path');
const merge = require('webpack-merge');
const parts = require('./webpack.parts');

const outputDir = path.resolve(__dirname, '..', '..', 'dist', 'webpacked');

const librarySkeleton = merge([
    {
        devtool: 'cheap-module-source-map',

        output: {
            path: outputDir,
            pathinfo: true,
            filename: 'js/[name].js'
        }
    },

    parts.versionReplace(),

    parts.transpileJs({
        exclude: /(node_modules|bower_components|js-vendor|compiled-soy)/,
        options: {
            cacheDirectory: true,
        },
    }),

    parts.loadSoy(),

    parts.loadFonts({
        options: {
            name: '[name].[ext]',
            outputPath: 'css/fonts/',
            publicPath: '../',
        }
    }),

    parts.loadImages({
        options: {
            name: '[name].[ext]',
            outputPath: 'css/images/',
            publicPath: '../',
        },
    }),

    parts.extractCss({
        options: {
            filename: 'css/[name].css'
        }
    }),

    parts.production()
]);

module.exports = {
    outputDir,
    librarySkeleton
};
