/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Extended expose-members-loader created by Author David Pelayo @ddpelayo
	Extended expose-loader created by Tobias Koppers @sokra
*/
const loaderUtils = require('loader-utils');
const SourceNode = require('source-map').SourceNode;
const SourceMapConsumer = require('source-map').SourceMapConsumer;
const FOOTER = '/*** EXPORTS FROM expose-exports-loader ***/\n';

function accesorString(value, modulePrefix) {
    const childProperties = value.split('.');
    const name = childProperties[0];
    const length = childProperties.length;
    let exposePropertyString = 'global';
    let modulePropertyString = name;
    let result = '';
    let i;

    for (i = 0; i < length; i++) {
        if (i > 0) {
            result += 'if(!' + exposePropertyString + ') ' + exposePropertyString + ' = {};\n';
            modulePropertyString += '[' + JSON.stringify(childProperties[i]) + ']';
        }

        exposePropertyString += '[' + JSON.stringify(childProperties[i]) + ']';
    }

    result += exposePropertyString + ' = (' + modulePropertyString + ');\n';
    result += modulePrefix(JSON.stringify(name)) + ' = (' + modulePropertyString + ');';

    return result;
}

function multipleAccesorString(value) {
    return accesorString(value, function (name) {
        return 'module.exports[' + name + ']';
    });
}

function singleAccesorString(value) {
    return accesorString(value, function () {
        return 'module.exports';
    });
}

module.exports = function (content, sourceMap) {
    if (this.cacheable) {
        this.cacheable();
    }

    const query = loaderUtils.getOptions(this) || {};
    const exports = [];
    const keys = Object.keys(query);

    if (keys.length === 1 && typeof query[keys[0]] === 'boolean') {
        exports.push(singleAccesorString(keys[0]));
    }
    else {
        keys.forEach(function (name) {
            exports.push(multipleAccesorString(name));
        });
    }

    if (sourceMap) {
        const currentRequest = loaderUtils.getCurrentRequest(this);
        const node = SourceNode.fromStringWithSourceMap(content, new SourceMapConsumer(sourceMap));

        node.add('\n\n' + FOOTER + exports.join('\n'));

        const result = node.toStringWithSourceMap({
            file: currentRequest
        });

        this.callback(null, result.code, result.map.toJSON());

        return;
    }

    return content + '\n\n' + FOOTER + exports.join('\n');
};
