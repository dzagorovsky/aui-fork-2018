var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.series(
    gat.load('i18n'),
    gat.load('dist/build'),
    gat.load('flatapp/copy'),
    gat.load('flatapp/soy-assets'),
    gat.load('flatapp/soy-build')
);
