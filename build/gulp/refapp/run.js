'use strict';

var gat = require('gulp-auto-task');
var mavenStdoutFilter = require('../../tasks/lib/maven-stdout-filter');
var path = require('path');

var opts = gat.opts();

module.exports = function refappRun (done) {
    var INTEGRATION_PATH = 'integration/refapp';
    var proc = require('child_process');
    var debug = opts.debug ? 'debug' : 'run';
    var args = [
        `amps:${debug}`,
        '-DskipTests',
        '-DskipAllPrompts=true',
        `-Djquery.version=${opts.jquery || '1.8.3'}`,
        `-Daui.location=${path.resolve('.')}`,
    ];

    if (opts.verbose) {
        console.log(`mvn ${args.join(' ')}`);
    }

    var cmd = proc.spawn('mvn', args, {
        cwd: INTEGRATION_PATH
    });

    cmd.stdout.on('data', mavenStdoutFilter(opts.verbose));
    cmd.stdout.on('data', function(data) {
        data = data.toString();

        if (data.indexOf('refapp started successfully') !== -1) {
            console.log(data);
        } else if (data.indexOf('JDWP exit error') !== -1) {
            console.error(data);
        }
    });

    cmd.on('close', done);
};
