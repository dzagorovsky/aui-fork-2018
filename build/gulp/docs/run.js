var docsOpts = require('../../lib/docs-opts');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

var opts = gat.opts();
var { host, port, path } = Object.assign(docsOpts, opts);

module.exports = gulp.series(
    gat.load('docs/build'),
    function docsWatchServe () {
        return gulp.src('.tmp/docs')
            .pipe(gulpWebserver({
                host,
                port,
                path,
                open: path,
                livereload: false,
            }));
    }
);
