var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.series(
    gat.load('soy'),
    gat.load('docs/metalsmith'),
    gat.load('docs/js'),
    gat.load('docs/less')
);
