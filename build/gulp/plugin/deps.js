'use strict';

var gulp = require('gulp');
var pluginJs = require('./js');
var pluginLess = require('./less');
var pluginI18n = require('./i18n');
var wrm = require('../wrm');

module.exports = gulp.series(
    wrm,
    pluginI18n,
    pluginJs,
    pluginLess
);
