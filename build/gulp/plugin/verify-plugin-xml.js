'use strict';

var path = require('path');
var spawn = require('child_process').spawn;

module.exports = function pluginVerifyXml (done) {
    var verifyXmlCommand = 'build/bin/verify-plugin-xml.sh';
    var verifyXmlArguments = [
        path.resolve(path.join('integration', 'plugin', 'src', 'main', 'resources', 'atlassian-plugin.xml')),
        path.resolve(path.join('integration', 'plugin', 'pom.xml'))
    ];

    var cmd = spawn(verifyXmlCommand, verifyXmlArguments);

    cmd.stdout.on('data', function (data) {
        console.log(data.toString().trim());
    });
    cmd.stderr.on('data', function (data) {
        console.error(data.toString().trim());
    });

    cmd.on('close', function(code) {
        if (code === 1) {
            done(new Error(`Plugin XML verification failed. Run "./${verifyXmlCommand} ${verifyXmlArguments.join(' ')}" and fix errors to proceed`));
        } else {
            done(code);
        }
    });
};
