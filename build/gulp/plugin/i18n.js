'use strict';

var gulp = require('gulp');
var gulpRename = require('gulp-rename');
var gulpTap = require('gulp-tap');
var gulpNative2Ascii = require('gulp-native2ascii');

function keysToProperties (keys) {
    return function (key) {
        return `${key}=${keys[key] != null ? keys[key] : ''}`;
    };
}

function streamToProperties () {
    return gulpTap(function (file) {
        var keys = require(file.path);
        var keyToProperty = keysToProperties(keys);
        file.contents = new Buffer(Object.keys(keys).map(keyToProperty).join('\n'));
    });
}

function renameToProperties (path) {
    path.extname = '.properties';
}

module.exports = function pluginI18n () {
    // The `lib` task must be run before this will work.
    return gulp.src('lib/js/aui/internal/i18n/*.js')
        .pipe(streamToProperties())
        .pipe(gulpNative2Ascii())
        .pipe(gulpRename(renameToProperties))
        .pipe(gulp.dest('.tmp/plugin/i18n'));
};
