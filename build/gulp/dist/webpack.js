const gulp = require('gulp');
const gat = require('gulp-auto-task');
const { buildWithWebpack } = require('../webpack');

const auiLib = require('../../webpack/dist.aui-lib.webpack.config');
const auiExtensions = require('../../webpack/dist.aui-extensions.webpack.config');
const auiSoy = require('../../webpack/dist.aui-soy.webpack.config');

const buildAuiLib = () => buildWithWebpack(auiLib);
const buildAuiExtensions = () => buildWithWebpack(auiExtensions);
const buildAuiSoy = () => buildWithWebpack(auiSoy);

module.exports = gulp.series(
    gulp.parallel(
        gat.load('dist/license'),
        buildAuiSoy(),
        gulp.series(buildAuiLib(), buildAuiExtensions())
    )
);
