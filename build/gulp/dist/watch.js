'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var rootPaths = require('../../lib/root-paths');

var taskDist = gat.load('dist/webpack');

module.exports = gulp.series(
    taskDist,
    function distWatch (done) {
        gulp.watch(rootPaths('src/**'), taskDist).on('change', galv.cache.expire);
        done();
    }
);
