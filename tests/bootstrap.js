import 'webcomponents.js/MutationObserver';
import helpers from './helpers/all';

let fixtureElement;

// Small AMD shim to test components that have been AMDified. We don't check
// define.amd but most libs do. If they don't check then they will end up
// calling this and may not export a global. This may cause problems, but since
// we don't have a good way to mock ES2015 modules yet, this will suffice.
window.defines = {};
window.define = function (name, deps, func) {
    if (typeof name === 'string') {
        this.defines[name] = func || deps;
    }
};
window.amdRequire = function (names, func) {
    func.apply(this, names.map(name => this.defines[name]()));
};

/**
 * Sets the el's innerHTML to '' and executes the next callback after any
 * DOM mutation handlers (e.g., skate's detached callbacks) have had a
 * chance to run.
 */
function clearContents(el, next) {
    el.innerHTML = '';
    if (next) {
        helpers.afterMutations(next);
    }
}

beforeEach(function () {
    fixtureElement = document.getElementById('test-fixture');
    if (!fixtureElement) {
        fixtureElement = document.createElement('div');
        fixtureElement.id = 'test-fixture';
        document.body.appendChild(fixtureElement);
    }
    clearContents(fixtureElement);
});

afterEach(function (done) {
    if (setTimeout.clock) {
        setTimeout.clock.restore();
    }

    clearContents(fixtureElement, function () {
        helpers.removeLayers();

        // Clean up after:
        // - blanket
        // - dialog
        // - dialog2
        // - tipsy
        $('body, html').css('overflow', '');
        $('.aui-blanket, .aui-dialog, .aui-popup [data-tether-id]').remove();
        $('.tipsy').remove();
        done();
    });
});
