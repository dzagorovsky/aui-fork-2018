'use strict';

import * as firebug from '../../../src/js/aui/firebug';

describe('aui/firebug', function () {
    it('exports', function () {
        expect(firebug).to.be.an('object');
        expect(firebug.warnAboutFirebug).to.be.a('function');
    });

    it('globals', function () {
        expect(AJS.firebug).to.be.defined;
    });
});
