'use strict';

import * as cookie from '../../../src/js/aui/cookie';

describe('aui/cookie', function () {
    it('globals', function () {
        expect(AJS.cookie).to.contain(cookie);
        expect(AJS.Cookie).to.contain(cookie);
    });
});
