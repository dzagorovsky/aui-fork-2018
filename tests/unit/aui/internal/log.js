'use strict';

import * as log from '../../../../src/js/aui/internal/log';

describe('aui/internal/log', function () {
    it('globals', function () {
        expect(AJS.log).to.be.equal(log.log);
        expect(AJS.error).to.be.equal(log.error);
        expect(AJS.warn).to.be.equal(log.warn);
    });
});
