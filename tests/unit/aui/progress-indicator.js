'use strict';

import progressBars from '../../../src/js/aui/progress-indicator';
import helpers from '../../helpers/all';
import skate from '../../../src/js/aui/internal/skate';

function createElement(htmlString) {
    const dom = helpers.fixtures({
        el: htmlString
    });
    skate.init(dom.el);
    return dom.el;
}

describe('aui/progress-indicator', function () {
    it('globals', function () {
        expect(AJS.progressBars).to.equal(progressBars);
    });

    /**
     * @deprecated since 7.7.0
     */
    describe('#update', function () {
        const barId = 'some-progress-bar';
        let barContainer;
        let barInner;

        beforeEach(function () {
            barContainer = createElement(`
                <div id="${barId}" class="aui-progress-indicator">
                    <span class="aui-progress-indicator-value"></span>
                </div>`);
            barInner = barContainer.querySelector('.aui-progress-indicator-value');
        });

        it('can update the progress by CSS selector', function () {
            progressBars.update(`#${barId}`, 0.2);
            expect(barContainer.getAttribute('data-value')).to.equal('0.2');
            expect(barInner.style.width).to.equal('20%');
        });

        // AUI-4771 - this case is described by the docs, but never worked. This is why tests are good!
        it('can update the progress by plain ID', function () {
            progressBars.update(barId, 0.2);
            expect(barContainer.getAttribute('data-value')).to.equal('0.2');
            expect(barInner.style.width).to.equal('20%');
        });

        it('can update the progress by element reference', function () {
            progressBars.update(barContainer, 0.2);
            expect(barContainer.getAttribute('data-value')).to.equal('0.2');
            expect(barInner.style.width).to.equal('20%');
        });

        // AUI-4771 - heal unexpected markup patterns.
        it('can render progress correctly when the initial markup is incorrect', function () {
            barContainer = createElement(`
                <div id="${barId}" class="aui-progress-indicator" data-value="0.72">
                    <span class="aui-progress-indicator-value"></span>
                </div>`);
            barInner = barContainer.querySelector('.aui-progress-indicator-value');
            progressBars.update(barContainer, 0.72);
            expect(barInner.style.width).to.equal('72%');
        });
    });
});

describe('<aui-progressbar>', function () {
    describe('by default', function () {
        let bar;
        beforeEach(function () {
            bar = createElement('<aui-progressbar></aui-progressbar>');
        });

        describe('it acts like html5 <progress>', function () {
            let html5Val = 0;
            let html5Max = 1;

            it(`has a value of ${html5Val}`, function () {
                expect(bar.value).to.equal(html5Val);
            });

            it(`has a max value of ${html5Max}`, function () {
                expect(bar.max).to.equal(html5Max);
            });
        });

        // Attributes and properties
        ['value', 'max'].forEach(function(thing) {
            describe(`${thing} property`, function () {
                it('accepts float values', function () {
                    bar[thing] = 0.22;
                    expect(bar[thing]).to.equal(0.22);
                });

                it('will not update if a non-integer value is given', function () {
                    bar[thing] = 0.22;
                    bar[thing] = 'foo';
                    expect(bar[thing]).to.equal(0.22);
                    bar[thing] = NaN;
                    expect(bar[thing]).to.equal(0.22);
                });

                it('has a precision of 6 decimal places', function() {
                    bar[thing] = 1 - (1 / 3);
                    expect(bar[thing]).to.equal(0.666667);
                });
            });

            describe(`${thing} attribute`, function () {
                it('updates the property when set', function (done) {
                    bar.setAttribute(thing, 0.22);
                    helpers.afterMutations(function () {
                        expect(bar[thing]).to.equal(0.22);
                        done();
                    });
                });

                it('accepts string-based numeric values', function (done) {
                    bar.setAttribute(thing, '0.44');
                    helpers.afterMutations(function () {
                        expect(bar[thing]).to.equal(0.44);
                        done();
                    });
                });

                it('does not update property with non-numeric values', function (done) {
                    bar.setAttribute(thing, 'bar');
                    helpers.afterMutations(function () {
                        expect(bar[thing]).to.not.equal('bar');
                        done();
                    });
                });

                it('leaves the last valid property value intact', function (done) {
                    bar.setAttribute(thing, 0.22);
                    helpers.afterMutations(function () {
                        bar.setAttribute(thing, 'bar');
                        helpers.afterMutations(function () {
                            expect(bar[thing]).to.equal(0.22);
                            done();
                        });
                    });
                });
            });
        });

        describe('the value', function () {
            it('cannot go below zero', function () {
                bar.value = -1;
                expect(bar.value).to.equal(0);
            });

            it('cannot exceed the implicit max value', function () {
                bar.value = bar.max + 1;
                expect(bar.value).to.equal(bar.max);
            });

            it('cannot exceed an explicit max value', function () {
                bar.max = 50;
                bar.value = 55;
                expect(bar.value).to.equal(bar.max);
            });

            it('is adjusted to max when the max is set below it', function () {
                bar.max = 50;
                bar.value = 45;
                expect(bar.value).to.equal(45);
                bar.max = 10;
                expect(bar.value).to.equal(10);
            });

            it('can be incremented to the max', function () {
                bar.value = 0;
                bar.value += 0.5;
                expect(bar.value).to.equal(0.5);
                bar.value += 0.5;
                expect(bar.value).to.equal(1);
                bar.value += 0.5;
                expect(bar.value).to.equal(1);
            });

            it('can be decremented to zero', function () {
                bar.value = 1;
                bar.value -= 0.5;
                expect(bar.value).to.equal(0.5);
                bar.value -= 0.5;
                expect(bar.value).to.equal(0);
                bar.value -= 0.5;
                expect(bar.value).to.equal(0);
            })
        });

        describe('the max', function () {
            it('cannot go below zero', function () {
                bar.max = -1;
                expect(bar.max).to.equal(1);
            });

            it('cannot be zero', function () {
                bar.max = 0;
                expect(bar.max).to.equal(1);
            });
        });
    });

    describe('when configured in html', function () {
        it('configures progress properties correctly', function () {
            let bar = createElement('<aui-progressbar value="7.5" max="42"></aui-progressbar>');
            expect(bar.value).to.equal(7.5);
            expect(bar.max).to.equal(42);
        });

        it('does not care about attribute order', function () {
            let bar = createElement('<aui-progressbar max="42" value="7.5"></aui-progressbar>');
            expect(bar.value).to.equal(7.5);
            expect(bar.max).to.equal(42);
        });

        it('respects the implicit max', function () {
            let bar = createElement('<aui-progressbar value="7.5"></aui-progressbar>');
            expect(bar.value).to.equal(1);
        });

        it('respects an explicit max', function () {
            let bar = createElement('<aui-progressbar value="7.5" max="5"></aui-progressbar>');
            expect(bar.value).to.equal(5);
        });

        it('ignores invalid values', function () {
            let bar = createElement('<aui-progressbar value="foo" max="NaN"></aui-progressbar>');
            expect(bar.value).to.equal(0);
            expect(bar.max).to.equal(1);
        });

        it('ignores an out of range value', function () {
            let bar = createElement('<aui-progressbar value="-1"></aui-progressbar>');
            expect(bar.value).to.equal(0);
        });

        it('ignores an out of range max', function () {
            let bar = createElement('<aui-progressbar max="-1"></aui-progressbar>');
            expect(bar.max).to.equal(1);
        });

        it('can start in the indeterminate state', function () {
            let bar = createElement('<aui-progressbar indeterminate value=1></aui-progressbar>');
            expect(bar.indeterminate).to.equal(true);
        });
    });

    describe('accessibility', function () {
        let bar;
        let indicator;

        beforeEach(function () {
            bar = createElement('<aui-progressbar value="1.5" max="2.4"></aui-progressbar>');
            indicator = bar.querySelector('.aui-progress-indicator');
        });

        it('outputs appropriate aria-attribute values', function () {
            expect(indicator.getAttribute('aria-valuemin')).to.equal('0');
            expect(indicator.getAttribute('aria-valuenow')).to.equal('1.5');
            expect(indicator.getAttribute('aria-valuemax')).to.equal('2.4');
        });

        it('updates when the property values are changed', function (done) {
            bar.value = 1.1;
            helpers.afterMutations(function () {
                expect(indicator.getAttribute('aria-valuenow')).to.equal('1.1');
                done();
            });
        });
    });
});
