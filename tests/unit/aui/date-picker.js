'use strict';

import '../../../src/js/aui';
import $ from '../../../src/js/aui/jquery';
import DatePicker from '../../../src/js/aui/date-picker';

describe('aui/date-picker', function () {
    function selectDate(date) {
        $(`.ui-datepicker-calendar td:contains(${date})`).click();
    }

    beforeEach(function () {
        $('#test-fixture').html('<input class="aui-date-picker" id="test-input" type="date">');
    });

    afterEach(function () {
        $('.aui-inline-dialog').remove();
    });

    it('globals', function () {
        expect(AJS.DatePicker).to.equal(DatePicker);
    });

    it('API', function () {
        expect(DatePicker).to.be.a('function');
        expect(DatePicker.prototype.browserSupportsDateField).to.be.a('boolean');
        expect(DatePicker.prototype.defaultOptions).to.be.an('object');
        expect(DatePicker.prototype.defaultOptions.overrideBrowserDefault).to.be.a('boolean');
        expect(DatePicker.prototype.defaultOptions.firstDay).to.be.a('number');
        expect(DatePicker.prototype.defaultOptions.languageCode).to.be.a('string');
        expect(DatePicker.prototype.localisations).to.be.an('object');
    });

    it('instance API (without polyfill)', function () {
        var datePicker;
        var input;

        DatePicker.prototype.browserSupportsDateField = true;

        input = $('#test-input');
        datePicker = input.datePicker();

        expect(datePicker).to.be.an('object');
        expect(datePicker.getField).to.be.a('function');
        expect(datePicker.getOptions).to.be.a('function');
        expect(datePicker.reset).to.be.a('function');
        expect(datePicker.hide).to.not.be.defined;
        expect(datePicker.show).to.not.be.defined;
        expect(datePicker.getDate).to.not.be.defined;
        expect(datePicker.setDate).to.not.be.defined;
        expect(datePicker.destroyPolyfill).to.not.be.defined;
    });

    it('instance API (with polyfill)', function () {
        var datePicker;
        var input;

        DatePicker.prototype.browserSupportsDateField = false;

        input = $('#test-input');
        datePicker = input.datePicker();

        expect(datePicker).to.be.an('object');
        expect(datePicker.getField).to.be.a('function');
        expect(datePicker.getDate).to.be.a('function');
        expect(datePicker.setDate).to.be.a('function');
        expect(datePicker.getOptions).to.be.a('function');
        expect(datePicker.reset).to.be.a('function');
        expect(datePicker.hide).to.be.a('function');
        expect(datePicker.show).to.be.a('function');
        expect(datePicker.destroyPolyfill).to.be.a('function');
    });

    it('change event fires (with polyfill)', function () {
        var input;
        var datePicker;
        var inputEventSpy;

        DatePicker.prototype.browserSupportsDateField = false;

        input = $('#test-input');
        inputEventSpy = sinon.spy();
        input.on('change', runSpy);

        //We need to wrap the spy because sinon uses 'this' to access and record properties
        //when called directly as an eventHandler the value of 'this' is the element and
        //in chrome there are some properties on HTMLInputElement that throw exceptions
        //when accessed.
        function runSpy(){
            inputEventSpy();
        }

        datePicker = input.datePicker();
        datePicker.show();
        selectDate('16');

        inputEventSpy.should.have.been.calledOnce;
    });
});
