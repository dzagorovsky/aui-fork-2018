'use strict';

import escapeHtml from '../../../src/js/aui/escape-html';

describe('aui/escape-html', function () {
    it('globals', function () {
        expect(AJS.escapeHtml).to.equal(escapeHtml);
    });

    it('API', function () {
        expect(escapeHtml('a \" doublequote')).to.equal('a &quot; doublequote');
        expect(escapeHtml('a \' singlequote')).to.equal('a &#39; singlequote');
        expect(escapeHtml('a < lessthan')).to.equal('a &lt; lessthan');
        expect(escapeHtml('a > greaterthan')).to.equal('a &gt; greaterthan');
        expect(escapeHtml('a & ampersand')).to.equal('a &amp; ampersand');
        expect(escapeHtml('a ` accent grave')).to.equal('a &#96; accent grave');
        expect(escapeHtml('foo')).to.equal('foo');
        expect(escapeHtml('<foo>')).to.equal('&lt;foo&gt;');
        expect(escapeHtml('as<foo>as')).to.equal('as&lt;foo&gt;as');
        expect(escapeHtml('some <input class=\"foo\" value=\'bar&wombat\'> thing')).to.equal('some &lt;input class=&quot;foo&quot; value=&#39;bar&amp;wombat&#39;&gt; thing');
    });
});
