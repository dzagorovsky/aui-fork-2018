'use strict';

import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import skate from '../../../src/js/aui/internal/skate';
import '../../../src/js/aui/button';
import '../../../src/js/aui/spin';

describe('aui/spin', () => {
    let $testFixture;
    const SPINNER_SELECTOR = 'aui-spinner';

    beforeEach(() => {
        $testFixture = $('#test-fixture');
    });

    describe('basic functionality - ', () => {
        let $buttonShow;
        let $buttonHide;
        let $spinner;

        beforeEach(() => {
            $buttonShow = $('<button id="show-spinner"></button>').appendTo($testFixture);
            $buttonHide = $('<button id="hide-spinner"></button>').appendTo($testFixture);
            $spinner = $('<span id="spinner"></span>').appendTo($testFixture);

            $buttonShow.on('click', $spinner.spin.bind($spinner));
            $buttonHide.on('click', $spinner.spinStop.bind($spinner));
        });

        it('should show spinner', () => {
            $buttonHide.click();
            $buttonShow.click();

            expect($(SPINNER_SELECTOR).length).to.equal(1);
        });

        it('should hide spinner', () => {
            $buttonShow.click();
            $buttonHide.click();

            expect($(SPINNER_SELECTOR).length).to.equal(0);
        });
    });

    describe('jQuery integration (deprecated) - ', () => {
        beforeEach(() => {
            $testFixture.append(`
                <span class="add-here"></span>
                <span class="not-here"></span>
                <span class="add-here"></span>
            `);
        });

        ['spin', 'spinStop'].forEach(method => {
            it(`${method}() should be tolerant of nonsensical selections`, () => {
                let $els = $([null, undefined, {}]);
                $els[method]();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });

            it(`${method}() should support jQuery function chaining`, () => {
                let $els = $testFixture.find('span');
                let result = $els[method]();
                expect(result).to.be.an.instanceof($);
            });
        });

        describe('spin() ', () => {
            it('should add a spinner to each selected element', () => {
                let $els = $testFixture.find('.add-here');
                $els.spin();
                expect($(SPINNER_SELECTOR).length).to.equal(2);
            });

            it('should not add spinners when no elements are selected', () => {
                let $els = $testFixture.find('.nonexistent');
                $els.spin();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });
        });

        describe('spinStop() ', () => {
            beforeEach(() => {
                $testFixture
                    .append('<span class="already-spinning"></span>')
                    .prepend('<span class="already-spinning"></span>');
                $testFixture.find('.already-spinning').spin();
                expect($(SPINNER_SELECTOR).length).to.equal(2);
            });

            it('should remove a spinner from each selected element', () => {
                let $els = $testFixture.find('.already-spinning');
                $els.spinStop();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });
        });

        // This is effectively the case whenever a spinner was added by a means other than calling .spin()
        describe('$.data() returned null', () => {
            let dataFn;
            let $spinner;

            beforeEach(() => {
                $spinner = $('<span id="spinner"></span>').appendTo($testFixture);
                $spinner.spin();
                dataFn = $.fn.data;
                $.fn.data = () => null;
            });

            it('should not hide previous spinner', () => {
                $spinner.spinStop();
                expect($(SPINNER_SELECTOR).length).to.equal(1);
            });

            afterEach(() => {
                $.fn.data = dataFn;
            });
        });
    });

    describe('busy button - ', () => {
        let button;

        beforeEach(() => {
            button = helpers
                .fixtures({ button: '<button class="aui-button">Some Button</button>' })
                .button;
            skate.init(button);
        });

        afterEach(() => {
            $(button).remove();
        });

        it('should show spinner', () => {
            button.busy();
            expect($(SPINNER_SELECTOR).length).to.equal(1);
        });

        it('should hide spinner', () => {
            button.idle();
            expect($(SPINNER_SELECTOR).length).to.equal(0);
        });
    });

    describe('correct position - ', () => {
        const SVG_SELECTOR = `${ SPINNER_SELECTOR }>.${ SPINNER_SELECTOR }>svg`;
        const NO_ANIMATION_STYLES = { 'animation': 'none', '-webkit-animation': 'none' };
        const SPINNER_SIZE = { height: 20, width: 20 };
        const CONTAINER_SIZE = { height: 100, width: 100 };
        const ZERO_CONTAINER_SIZE = { height: 0, width: 0 };
        const PADDING = 5;
        const MARGIN = 5;

        let $container;
        let $containerZeroSize;
        let $containerZeroSizeWithMargin;

        function getSVG() {
            const $svg = $(SVG_SELECTOR);
            return {
                $svg,
                turnAnimationOff() {
                    $svg.css(NO_ANIMATION_STYLES);
                    return this;
                },
                getPosition() {
                    return $svg.position();
                }
            }
        }

        function getExpectedSpinnerPosition(containerSize, padding = 0, margin = 0) {
            return {
                expectedLeft: (containerSize.width - SPINNER_SIZE.width) / 2 + (padding + margin),
                expectedTop: (containerSize.height - SPINNER_SIZE.height) / 2 + (padding + margin)
            }
        }

        beforeEach(() => {
            $container = $(`<div style="width: ${CONTAINER_SIZE.width}px; height: ${CONTAINER_SIZE.height}px; display: inline-block; position: absolute; top: 0; left: 0"></div>`);
            $testFixture.append($container);

            $containerZeroSize = $(`<div style="width: ${ZERO_CONTAINER_SIZE.width}px; height: ${ZERO_CONTAINER_SIZE.height}px"></div>`);
            $testFixture.append($containerZeroSize);

            $containerZeroSizeWithMargin =
                $(`<div style="width: ${ZERO_CONTAINER_SIZE.width}px; height: ${ZERO_CONTAINER_SIZE.height}px; padding: ${PADDING}px">
                    <div style="margin: ${MARGIN}px">
                        <aui-spinner filled size="small"></aui-spinner>
                    </div>
                </div>`);
        });

        it('should set correct spin position for simple div box', done => {
            $container.spin();
            helpers.afterMutations(() => {
                const { left, top } = getSVG().turnAnimationOff().getPosition();
                const { expectedLeft, expectedTop } = getExpectedSpinnerPosition(CONTAINER_SIZE);

                expect(left).to.equal(expectedLeft);
                expect(top).to.equal(expectedTop);
                done();
            });
        });

        it('should set correct spin position for div with zero size', done => {
            $containerZeroSize.spin();
            helpers.afterMutations(() => {
                const { left, top } = getSVG().turnAnimationOff().getPosition();
                const { expectedLeft, expectedTop } = getExpectedSpinnerPosition(ZERO_CONTAINER_SIZE);

                expect(left).to.equal(expectedLeft);
                expect(top).to.equal(expectedTop);
                done();
            });
        });

        // simulating spinner behavior in dropdown
        it('should set correct spin position for div with padding inside div with margin', done => {
            $testFixture.append($containerZeroSizeWithMargin);
            helpers.afterMutations(() => {
                const { left, top } = getSVG().turnAnimationOff().getPosition();
                const { expectedLeft, expectedTop } = getExpectedSpinnerPosition(ZERO_CONTAINER_SIZE, PADDING, MARGIN);

                expect(left).to.equal(expectedLeft);
                expect(top).to.equal(expectedTop);
                done();
            });
        });
    });
});
