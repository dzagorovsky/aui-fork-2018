'use strict';

import filterBySearch from '../../../src/js/aui/filter-by-search';

describe('aui/filter-by-search', function () {
    it('globals', function () {
        expect(AJS.filterBySearch).to.equal(filterBySearch);
    });

    it('API', function () {
        var entries = [{
            'name': 'potato',
            'keywords': ['potato', 'mashed', 'chip', 'baked']
        }, {
            'name': 'chocolate',
            'keywords': ['chocolate', 'biscuits', 'bar', 'chip']
        }, {
            'name': 'venues',
            'keywords': ['venues', 'pub', 'bar', 'club']
        }];

        // Searching for x should return item with names y
        function assertResult(search, itemNames) {
            var result = filterBySearch(entries, search);
            var resultNames = [];

            result.forEach(function (entry) {
                resultNames.push(entry.name);
            });

            expect(resultNames).to.deep.equal(itemNames);
        }

        assertResult('', []);
        assertResult('potato', ['potato']);
        assertResult('bar', ['chocolate', 'venues']);
        assertResult('chip', ['potato', 'chocolate']);
        assertResult('chip bar', ['chocolate']);
    });
});
