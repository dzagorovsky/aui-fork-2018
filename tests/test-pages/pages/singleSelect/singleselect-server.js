define(function(require) {
    var sinon = require('sinon');

    var server = sinon.fakeServer.create();
    server.autoRespond = true;
    server.autoRespondAfter = 1000;
    server.respondWith(/bar-drinks/, [
        200,
        {'Content-Type': 'application/json'},
        '[{ "label": "Cement Mixer 1" }, { "label": "Mind Eraser 1" }, { "label": "Another drink", "value": "another-drinky-poo" }]'
    ]);

    server.respondWith(/library/, [
        200,
        {'Content-Type': 'application/json'},
        '[{ "label": "Sizzle", "img-src": "sizzle.png" }, { "label": "Bower", "img-src": "bower.png" }]'
    ]);

    server.respondWith(/stars/, [
        200,
        {'Content-Type': 'application/json'},
        '[{ "label": "Sol" }, { "label": "Achernar" }, { "label": "Achird" }, { "label": "Kajam" }, { "label": "Ras Thaoum" }, { "label": "Rastaban" }]'
    ]);
});
