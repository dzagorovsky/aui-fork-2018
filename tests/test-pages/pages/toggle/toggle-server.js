define(function(require, exports) {
    var sinon = require('sinon');

    exports.url = '/toggle';
    exports.server = sinon.fakeServer.create();
    exports.server.autoRespond = true;
    exports.server.autoRespondAfter = 200;

    exports.server.respondWith('POST', exports.url, [
        200,
        { 'Content-Type': 'application/json' },
        ''
    ]);
});
