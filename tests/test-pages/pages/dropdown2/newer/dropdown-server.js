define(function(require) {
    var sinon = require('sinon');
    var { serverResponse, noSectionLabelResponse, opensSubmenuResponse } = require('./dropdown-fixtures');

    var server = sinon.fakeServer.create();
    server.autoRespond = true;
    server.autoRespondAfter = 2000;

    server.respondWith(/standard-async-dropdown/, [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(serverResponse)
    ]);

    server.respondWith(/no-section-label/, [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(noSectionLabelResponse)
    ]);

    server.respondWith(/opens-submenu/, [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(opensSubmenuResponse)
    ]);

    return server;
});
