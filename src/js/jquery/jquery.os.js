jQuery.os = {};
(function () {
    var platform = navigator.platform.toLowerCase();
    jQuery.os.windows = (platform.indexOf('win') != -1);
    jQuery.os.mac = (platform.indexOf('mac') != -1);
    jQuery.os.linux = (platform.indexOf('linux') != -1);
}());
