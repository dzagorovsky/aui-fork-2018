'use strict';

import $ from './jquery';
import './spinner';

/**
 * @param start The argument can take only FALSE, in this case, the spinner will be stopped.
 */
$.fn.spin = function spinStart(start = true) {
    if (start === false) {
        return this.spinStop();
    }
    return this.each(function() {
        if (!this || !this.nodeType) { return; }
        const $this = $(this);
        const data = $this.data();
        if (data) {
            const $spinnerDom = $('<aui-spinner size="small" filled></aui-spinner>');

            $this.spinStop();
            $this.append($spinnerDom);

            data.spinner = $spinnerDom;
        }
    });
};

$.fn.spinStop = function spinStop() {
    return this.each(function() {
        if (!this || !this.nodeType) { return; }
        const $this = $(this);
        const data = $this.data();
        if (data && data.spinner) {
            data.spinner.remove();
        }
    });
};
