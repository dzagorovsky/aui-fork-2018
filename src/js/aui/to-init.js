'use strict';

import $ from './jquery';
import * as logger from './internal/log';
import globalize from './internal/globalize';

/**
 * Adds functions to the list of methods to be run on initialisation. Wraps
 * error handling around the provided function so its failure won't prevent
 * other init functions running.
 *
 * @param {Function} func Function to be call on initialisation.
 *
 * @return {Object}
 */
function toInit (func) {
    $(function () {
        try {
            func.apply(this, arguments);
        } catch (ex) {
            logger.log('Failed to run init function: ' + ex + '\n' + func.toString());
        }
    });

    return this;
}

globalize('toInit', toInit);

export default toInit;
