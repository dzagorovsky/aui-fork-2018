package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.SoyTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since 4.0
 */
public class AUISoyTest extends AbstractAuiIntegrationTest
{

    private SoyTestPage soyTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        soyTestPage = product.visit(SoyTestPage.class);
        elementFinder = soyTestPage.getElementFinder();
    }

    private void assertHasClass(PageElement el, String cssClass)
    {
        assertTrue("Should have class '" + cssClass + "'", el.hasClass(cssClass));
    }

    private void assertTextEquals(PageElement el, String text)
    {
        assertEquals("Inner text should be '" + text + "'", text, el.getText());
    }

    private void assertAttributeEquals(PageElement el, String attr, String expectedValue)
    {
        String val = el.getAttribute(attr);
        assertEquals(attr + " should be '" + expectedValue + "'", expectedValue, val);
    }

    private void assertHashEquals(PageElement el, String attr, String hash)
    {
        String attrVal = el.getAttribute(attr);
        assertEquals(attr + " should be '" + hash + "'", hash, attrVal.substring(attrVal.indexOf("#")));
    }

    private void assertHashEquals(PageElement el, String hash)
    {
        assertHashEquals(el, "href", hash);
    }

    private void assertElementExists(PageElement el)
    {
        assertTrue("Element should exist", el.isPresent());
    }

    private void assertHasRootElementProperties(PageElement element, String expectedId, String expectedTagName)
    {
        assertElementExists(element);
        if (expectedId != null) {
            assertEquals("Root element should have id '" + expectedId + "'.", expectedId, element.getAttribute("id"));
        }

        assertHasClass(element, "extra-class");
        assertEquals("Root element should have attribute 'data-attr' set to 'extra-attr'", "extra-attr", element.getAttribute("data-attr"));

        if (expectedTagName != null) {
            assertEquals("Root element should have tagName '" + expectedTagName + "'.", expectedTagName, element.getTagName());
        }
    }

    @Test
    public void testPagePanel()
    {
        PageElement pagePanel = elementFinder.find(By.id("soy-complex-page-panel"));
        PageElement pagePanelNav = elementFinder.find(By.id("soy-complex-page-panel-nav"));
        PageElement pagePanelContent = elementFinder.find(By.id("soy-complex-page-panel-content"));
        PageElement pagePanelSidebar = elementFinder.find(By.id("soy-complex-page-panel-sidebar"));
        PageElement pagePanelItem = elementFinder.find(By.id("soy-complex-page-panel-item"));

        // Panel
        assertHasRootElementProperties(pagePanel, "soy-complex-page-panel", "section");
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-panel.aui-page-panel > .aui-page-panel-inner > .aui-page-panel-nav")));
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-panel.aui-page-panel > .aui-page-panel-inner > .aui-page-panel-content")));
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-panel.aui-page-panel > .aui-page-panel-inner > .aui-page-panel-sidebar")));
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-panel.aui-page-panel > .aui-page-panel-inner > .aui-page-panel-item")));

        // Nav
        assertHasRootElementProperties(pagePanelNav, "soy-complex-page-panel-nav", "section");
        assertTextEquals(pagePanelNav, "AUI Page Panel Nav (using a section element instead of the default of div)");

        // Content
        assertHasRootElementProperties(pagePanelContent, "soy-complex-page-panel-content", "aside");
        assertTextEquals(pagePanelContent, "AUI Page Panel Content (using a aside element instead of the default of section)");

        // Sidebar
        assertHasRootElementProperties(pagePanelSidebar, "soy-complex-page-panel-sidebar", "nav");
        assertTextEquals(pagePanelSidebar, "AUI Page Panel Sidebar (using a nav element instead of the default of aside)");

        // Item
        assertHasRootElementProperties(pagePanelItem, "soy-complex-page-panel-item", "nav");
        assertTextEquals(pagePanelItem, "AUI Page Panel Item (using a nav element instead of the default of section)");

    }

    @Test
    public void testPageHeader()
    {
        PageElement pageHeader = elementFinder.find(By.id("soy-complex-page-header"));
        PageElement pageHeaderImage = elementFinder.find(By.id("soy-complex-page-header-image"));
        PageElement pageHeaderMain = elementFinder.find(By.id("soy-complex-page-header-main"));
        PageElement pageHeaderActions = elementFinder.find(By.id("soy-complex-page-header-actions"));

        // Header
        assertHasRootElementProperties(pageHeader, "soy-complex-page-header", "header");
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-header.aui-page-header > .aui-page-header-inner > .aui-page-header-image")));
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-header.aui-page-header > .aui-page-header-inner > .aui-page-header-main")));
        assertElementExists(elementFinder.find(By.cssSelector("#soy-complex-page-header.aui-page-header > .aui-page-header-inner > .aui-page-header-actions")));

        // Image
        assertHasRootElementProperties(pageHeaderImage, "soy-complex-page-header-image", "div");
        assertTextEquals(pageHeaderImage, "AUI Page Header Image");

        // Main
        assertHasRootElementProperties(pageHeaderMain, "soy-complex-page-header-main", "div");
        assertTextEquals(pageHeaderMain, "AUI Page Header Main");

        // Actions
        assertHasRootElementProperties(pageHeaderActions, "soy-complex-page-header-actions", "div");
        assertTextEquals(pageHeaderActions, "AUI Page Header Actions");

    }

    @Test
    public void testComplexPanel()
    {
        PageElement complexPanel = elementFinder.find(By.id("soy-panel"));
        assertHasRootElementProperties(complexPanel, "soy-panel", "section");
        assertTextEquals(complexPanel, "AUI Panel &");
    }

    @Test
    public void testComplexGroup()
    {
        PageElement complexGroup = elementFinder.find(By.id("soy-group"));
        assertHasRootElementProperties(complexGroup, "soy-group", "section");
        assertHasClass(complexGroup, "aui-group-split");

        PageElement complexItem1 = elementFinder.find(By.id("soy-item-1"));
        assertHasRootElementProperties(complexItem1, "soy-item-1", "section");
        assertTextEquals(complexItem1, "AUI Item 1 in AUI Group &");
    }

    @Test
    public void testComplexTable()
    {
        PageElement complexTable = elementFinder.find(By.id("soy-table"));
        assertHasRootElementProperties(complexTable, "soy-table", null);

        PageElement column = complexTable.find(By.id("complex-table-column"));
        assertEquals("Specified column is present", "col", column.getTagName());

        PageElement tbody = complexTable.find(By.cssSelector("tbody"));
        assertTextEquals(tbody, "AUI Table (tbody) &");

        PageElement thead = complexTable.find(By.cssSelector("thead"));
        assertTextEquals(thead, "AUI Table (thead) &");

        PageElement tfoot = complexTable.find(By.cssSelector("tfoot"));
        assertTextEquals(tfoot, "AUI Table (tfoot) &");

        PageElement caption = complexTable.find(By.cssSelector("caption"));
        assertTextEquals(caption, "AUI Table (caption) &");
    }

    @Test
    public void testComplexTabs()
    {
        PageElement complexTabs = elementFinder.find(By.id("soy-tabs"));
        assertHasRootElementProperties(complexTabs, "soy-tabs", "section");
        assertHasClass(complexTabs, "vertical-tabs");
        assertHasClass(complexTabs, "aui-tabs-disabled");

        PageElement tabMenuItem1 = complexTabs.find(By.id("soy-tab-menu-1"));
        assertHasRootElementProperties(tabMenuItem1, "soy-tab-menu-1", null);
        assertHasClass(tabMenuItem1, "active-tab");
        assertHashEquals(tabMenuItem1.find(By.tagName("a")), "#soy-tab-pane-1");
        assertTextEquals(tabMenuItem1, "Tab 1 &amp;");

        PageElement tabPane1 = complexTabs.find(By.id("soy-tab-pane-1"));
        assertHasRootElementProperties(tabPane1, "soy-tab-pane-1", "section");
        assertHasClass(tabPane1, "active-pane");
        assertTextEquals(tabPane1, "Tab 1 Content - Disabled tabs &");
    }

    @Test
    public void testComplexDropdown()
    {
        PageElement complexDropdownParent = elementFinder.find(By.id("soy-dropdown-parent"));
        assertHasRootElementProperties(complexDropdownParent, "soy-dropdown-parent", "section");

        PageElement trigger = complexDropdownParent.find(By.id("soy-dropdown-trigger"));
        assertHasRootElementProperties(trigger, "soy-dropdown-trigger", null);
        assertTextEquals(trigger, "AUI Dropdown Trigger &amp;");

        PageElement triggerIcon = trigger.find(By.cssSelector(".icon"));
        assertTrue("Should not have an icon", !triggerIcon.isPresent());

        trigger.click();

        PageElement menu = complexDropdownParent.find(By.id("soy-dropdown-menu"));
        assertHasRootElementProperties(menu, "soy-dropdown-menu", "section");

        PageElement dropdownItem = menu.find(By.id("soy-dropdown-item"));
        assertHasRootElementProperties(dropdownItem, "soy-dropdown-item", "section");
        assertTextEquals(dropdownItem, "AUI Dropdown Item &amp;");
        assertHashEquals(dropdownItem.find(By.tagName("a")), "#dropdown-item");
    }

    @Test
    public void testComplexToolbar()
    {
        PageElement complexToolbar = elementFinder.find(By.id("soy-toolbar"));
        assertHasRootElementProperties(complexToolbar, "soy-toolbar", "section");

        PageElement splitLeft = complexToolbar.find(By.id("soy-toolbar-split-left"));
        assertHasRootElementProperties(splitLeft, "soy-toolbar-split-left", "section");
        assertHasClass(splitLeft, "toolbar-split-left");

        PageElement group = splitLeft.find(By.id("soy-toolbar-group"));
        assertHasRootElementProperties(group, "soy-toolbar-group", null);

        //Button 1

        PageElement button1 = group.find(By.id("soy-toolbar-button-1"));
        assertHasRootElementProperties(button1, "soy-toolbar-button-1", null);
        assertHasClass(button1, "active");
        assertHasClass(button1, "primary");

        PageElement button1Trigger = button1.find(By.className("toolbar-trigger"));
        assertHashEquals(button1Trigger, "#toolbar-button");
        assertTextEquals(button1Trigger, "AUI Button (text) &amp;");

        //Button 2

        PageElement button2 = group.find(By.id("soy-toolbar-button-2"));
        PageElement button2icon = button2.find(By.cssSelector(".toolbar-trigger .icon"));
        assertHasClass(button2icon, "my-toolbar-icon");

        //Link

        PageElement link = group.find(By.id("soy-toolbar-link"));
        assertHasRootElementProperties(link, "soy-toolbar-link", null);
        assertHashEquals(link.find(By.className("toolbar-trigger")), "#link");
        assertTextEquals(link, "AUI Link &amp;");

        //Dropdown

        PageElement dropdown = group.find(By.id("soy-toolbar-dropdown"));
        assertHasRootElementProperties(dropdown, "soy-toolbar-dropdown", null);

        PageElement dropdownTrigger = dropdown.find(By.className("toolbar-trigger"));
        assertTextEquals(dropdownTrigger, "AUI Toolbar Dropdown &amp;");
        dropdownTrigger.click();
        assertTextEquals(dropdown.find(By.className("dropdown-item")), "AUI Dropdown Item");

        //Split Button

        PageElement splitButton = group.find(By.id("soy-toolbar-split-button"));
        assertHasRootElementProperties(splitButton, "soy-toolbar-split-button", null);

        PageElement splitButtonTrigger = splitButton.find(By.cssSelector("#soy-toolbar-split-button > .toolbar-trigger"));
        assertTextEquals(splitButtonTrigger, "AUI Toolbar Split-Button &amp;");

        PageElement splitDropdownTrigger = splitButton.find(By.cssSelector("#soy-toolbar-split-button .aui-dd-parent .toolbar-trigger"));
        splitDropdownTrigger.click();
        assertTextEquals(splitButton.find(By.className("dropdown-item")), "AUI Dropdown Item");
    }

    private PageElement findFirstXContainingY(PageElementFinder finder, By x, By y) {
        for(PageElement thingToFind : finder.findAll(x)) {
            if (thingToFind.find(y).isPresent()) {
                return thingToFind;
            }
        }
        return null;
    }

    private PageElement findFieldsetForId(PageElementFinder finder, String id) {
        return findFirstXContainingY(finder, By.tagName("fieldset"), By.id(id));
    }

    private PageElement findFieldGroupForId(PageElementFinder finder, String id) {
        return findFirstXContainingY(finder, By.className("field-group"), By.id(id));
    }

    private PageElement findContainerForCheckboxOrRadio(PageElementFinder finder, String type, String id) {
        return findFirstXContainingY(finder, By.className(type), By.id(id));
    }

    private void assertField(PageElement fieldGroup, String id, String type, String rawText, String escapedText) {
        assertTrue("Field group should be found for #" + id, fieldGroup.isPresent());
        assertHasRootElementProperties(fieldGroup, null, null);

        PageElement label = fieldGroup.find(By.tagName("label"));
        assertAttributeEquals(label, "for", id);
        assertTrue("Label has required icon", label.find(By.className("icon-required")).isVisible());
        assertTextEquals(label, escapedText);

        PageElement input = fieldGroup.find(By.id(id));
        String tagName = input.getTagName();
        if ("input".equalsIgnoreCase(tagName)) {
            if (Arrays.asList("text", "password", "submit", "button").contains(type)) {
                assertAttributeEquals(input, "value", rawText);
            }
            assertAttributeEquals(input, "type", type);
        } else if ("textarea".equalsIgnoreCase(tagName) || "span".equalsIgnoreCase(tagName)) {
            assertTextEquals(input, rawText);
        } else if ("select".equalsIgnoreCase(tagName)) {
            assertTrue("Select should have the multiple attribute.", input.getAttribute("multiple") != null);
        }
        if (!"value".equals(type)) {
            assertAttributeEquals(input, "name", id + "-name");
            assertTrue("Input should have the disabled attribute.", input.getAttribute("disabled") != null);
        }
        assertHasClass(input, "password".equals(type) ? "text" :
                              "submit".equals(type) ? "button" :
                              "value".equals(type) ? "field-value" :
                              "select".equals(type) ? "multi-select" :
                              type);

        PageElement description = fieldGroup.find(By.className("description"));
        assertTextEquals(description, rawText);

        PageElement error = fieldGroup.find(By.className("error"));
        assertTextEquals(error, rawText);
    }

    private void assertCheckboxOrRadioField(PageElement fieldset, String id, String type, String rawText, String escapedText, String item1RawText, String item1EscapedText) {
        assertHasRootElementProperties(fieldset, id, null);
        assertHasClass(fieldset, "group");

        PageElement legend = fieldset.find(By.tagName("legend"));
        assertTrue("Legend has required icon", legend.find(By.className("icon-required")).isPresent());
        assertTextEquals(legend, escapedText);

        PageElement matrix = fieldset.find(By.className("matrix"));

        PageElement item1Container = findContainerForCheckboxOrRadio(fieldset, type, id + "-1");
        assertHasRootElementProperties(item1Container, null, null);
        assertHasClass(item1Container, type);

        PageElement item1 = item1Container.find(By.id(id + "-1"));
        assertHasClass(item1, type);
        assertAttributeEquals(item1, "name", "checkbox".equals(type) ? id + "-1-name" : id + "-name");
        assertAttributeEquals(item1, "checked", "true");

        PageElement item1Label = item1Container.find(By.tagName("label"));
        assertAttributeEquals(item1Label, "for", id + "-1");
        assertTextEquals(item1Label, item1EscapedText);

        PageElement item1Description = item1Container.find(By.className("description"));
        assertTextEquals(item1Description, item1RawText);

        PageElement item1Error = item1Container.find(By.className("error"));
        assertTextEquals(item1Error, item1RawText);

        PageElement errorsAndDescription = fieldset.find(By.className("field-group"));

        PageElement description = errorsAndDescription.find(By.className("description"));
        assertTextEquals(description, rawText);

        PageElement error = errorsAndDescription.find(By.className("error"));
        assertTextEquals(error, rawText);
    }

    @Test
    public void testComplexForm() {
        PageElement form = elementFinder.find(By.id("complex-form"));
        assertHasRootElementProperties(form, "complex-form", null);
        assertHashEquals(form, "action", "#form");
        //assertAttributeEquals(form, "method", "GET"); // fails but attribute is correct
        assertAttributeEquals(form, "enctype", "text/plain");
        assertHasClass(form, "unsectioned");
        assertHasClass(form, "long-label");
        assertHasClass(form, "top-label");

        PageElement description = form.find(By.id("complex-description"));
        assertHasRootElementProperties(description, "complex-description", null);
        assertTextEquals(description, "AUI Form &");

        PageElement fieldset = form.find(By.id("complex-fieldset"));
        assertHasRootElementProperties(fieldset, "complex-fieldset", null);
        assertHasClass(fieldset, "inline");

        PageElement textField = findFieldGroupForId(form, "complex-text-field");
        assertField(textField, "complex-text-field", "text", "Text Field &amp;", "Text Field &");

        PageElement textInput = textField.find(By.id("complex-text-field"));
        assertAttributeEquals(textInput, "maxlength", "16");
        assertAttributeEquals(textInput, "size", "18");

        PageElement textareaField = findFieldGroupForId(form, "complex-textarea-field");
        assertField(textareaField, "complex-textarea-field", "textarea", "Textarea Field &amp;", "Textarea Field &");

        PageElement textarea = textareaField.find(By.id("complex-textarea-field"));
        assertAttributeEquals(textarea, "rows", "2");
        assertAttributeEquals(textarea, "cols", "20");

        PageElement passwordField = findFieldGroupForId(form, "complex-password-field");
        assertField(passwordField, "complex-password-field", "password", "Password Field &amp;", "Password Field &");

        PageElement fileField = findFieldGroupForId(form, "complex-file-field");
        assertField(fileField, "complex-file-field", "file", "File Field &amp;", "File Field &");

        PageElement selectField = findFieldGroupForId(form, "complex-select-field");
        assertField(selectField, "complex-select-field", "select", "Select Field &amp;", "Select Field &");

        PageElement select = selectField.find(By.id("complex-select-field"));
        assertAttributeEquals(select, "size", "3");

        PageElement checkboxFieldset = findFieldsetForId(fieldset, "complex-checkbox-field");
        assertCheckboxOrRadioField(checkboxFieldset, "complex-checkbox-field", "checkbox", "Checkbox Field &amp;", "Checkbox Field &", "Checkbox 1 &amp;", "Checkbox 1 &");

        PageElement radioFieldset = findFieldsetForId(fieldset, "complex-radio-field");
        assertCheckboxOrRadioField(radioFieldset, "complex-radio-field", "radio", "Radio Field &amp;", "Radio Field &", "Radio 1 &amp;", "Radio 1 &");

        PageElement valueField = findFieldGroupForId(form, "complex-value-field");
        assertField(valueField, "complex-value-field", "value", "Value Field &amp;", "Value Field &");

    }

    public void assertComplexAuiMessage(String type) {
        PageElement complexMessage = elementFinder.find(By.id("soy-message-" + type));
        assertHasRootElementProperties(complexMessage, "soy-message-" + type, "section");

        PageElement messageTitle = elementFinder.find(By.cssSelector("#soy-message-" + type + " .title"));
        assertTextEquals(messageTitle, "AUI Message (" + type + ") &");

        PageElement messageContents = elementFinder.find(By.id("soy-message-" + type + "-content"));
        assertTextEquals(messageContents, "AUI Message (" + type + ") &");
    }

    @Test
    public void testComplexMessages() {
        assertComplexAuiMessage("success");
        assertComplexAuiMessage("error");
        assertComplexAuiMessage("hint");
        assertComplexAuiMessage("info");
        assertComplexAuiMessage("warning");
        assertComplexAuiMessage("generic");
    }

    @Test
    public void testHeaderCustomClasses()
    {
        PageElement customAttrHeader = elementFinder.find(By.id("header-customclassesandattrs"));
        assertHasClass(customAttrHeader, "my-awesome-custom-class");
        assertHasClass(customAttrHeader, "and-another");
    }

    @Test
    public void testHeaderCustomAttributes()
    {
        PageElement customAttrHeader = elementFinder.find(By.id("header-customclassesandattrs"));
        assertAttributeEquals(customAttrHeader, "data-foo", "bar");
        assertAttributeEquals(customAttrHeader, "title", "Custom title");
    }

    @Test
    public void testHeaderLogo()
    {
        PageElement headerLogo = elementFinder.find(By.cssSelector("#header-customclassesandattrs .aui-header-logo-bamboo .aui-header-logo-device"));
        assertTextEquals(headerLogo, "Bamboo");
    }

    @Test
    public void testHeaderExtraText()
    {
        PageElement customAttrHeaderExtraText = elementFinder.find(By.cssSelector("#header-customclassesandattrs .aui-header-logo-text"));
        assertTextEquals(customAttrHeaderExtraText, "This .aui-header has custom classes and attributes");
    }

    @Test
    public void testHeaderCustomImg()
    {
        PageElement customHeaderLogo = elementFinder.find(By.cssSelector("#header-customlogo img"));
        assertAttributeEquals(customHeaderLogo, "alt", "Bamboo");
        String headerLogoSrc = customHeaderLogo.getAttribute("src");
        assertTrue("src should contain img-logo-test40pxhigh.png", headerLogoSrc.contains("img-logo-test40pxhigh.png"));
    }

    @Test
    public void testHeaderPrimaryAndSecondary()
    {
        PageElement primaryNavContents = elementFinder.find(By.cssSelector("#header-customclassesandattrs .aui-header-primary #primaryNavContents"));
        assertTrue(".aui-header-primary should exist and contain #primaryNavContents", primaryNavContents.isVisible());

        PageElement secondaryNavContents = elementFinder.find(By.cssSelector("#header-customclassesandattrs .aui-header-secondary #secondaryNavContents"));
        assertTrue(".aui-header-secondary should exist and contain #secondaryNavContents", secondaryNavContents.isVisible());
    }

    @Test
    public void testButtonsWrapper()
    {
        PageElement buttonsWrapper = elementFinder.find(By.id("button-set-soy"));
        assertHasRootElementProperties(buttonsWrapper, "button-set-soy", "div");
        assertHasClass(buttonsWrapper, "aui-buttons");
    }

    public void assertComplexAuiButton(String buttonId, String tagName, String buttonType, String ariaPressed, String ariaDisabled)
    {
        PageElement complexAuiButton = elementFinder.find(By.id(buttonId));
        assertHasRootElementProperties(complexAuiButton, buttonId, tagName);
        assertAttributeEquals(complexAuiButton, "aria-pressed", ariaPressed);
        assertAttributeEquals(complexAuiButton, "aria-disabled", ariaDisabled);
        if (buttonType != null && buttonType != "default") {
            assertHasClass(complexAuiButton, "aui-button-" + buttonType);
        }
    }

    @Test
    public void testButtons()
    {
        assertComplexAuiButton("button-set-button1", "button", "default", "true", "true");
        assertComplexAuiButton("button-set-button2", "button", "primary", "true", "true");
        assertComplexAuiButton("button-set-button3", "input", "primary", "true", "true");
        assertComplexAuiButton("button-set-button4", "input", "primary", "true", "true");
        assertComplexAuiButton("button-set-button5", "a", "default", "true", "true");
        assertComplexAuiButton("button-set-button6", "button", "default", "true", "false");
        assertComplexAuiButton("button-set-button7", "button", "default", "true", "false");
        assertComplexAuiButton("button-set-button8", "input", "default", "true", "false");
        assertComplexAuiButton("button-set-button9", "input", "default", "true", "false");
        assertComplexAuiButton("button-set-button10", "a", "default", "true", "false");
        assertComplexAuiButton("button-set-button11", "a", "link", "false", "false");
        assertComplexAuiButton("button-set-button12", "a", "link", "true", "false");

        PageElement disabledButton3 = elementFinder.find(By.id("button-set-button3"));
        assertAttributeEquals(disabledButton3, "type", "button");
        PageElement disabledButton4 = elementFinder.find(By.id("button-set-button4"));
        assertAttributeEquals(disabledButton4, "type", "submit");
    }

    @Test
    public void testButtonText()
    {
        PageElement buttonText1 = elementFinder.find(By.id("button-text-test-1"));
        assertElementExists(buttonText1);
        assertTextEquals(buttonText1, "Button Text");

        PageElement buttonText2 = elementFinder.find(By.id("button-text-test-2"));
        assertElementExists(buttonText2);
        assertTextEquals(buttonText2, "Button Text");

        PageElement buttonText3 = elementFinder.find(By.id("button-text-test-3"));
        assertElementExists(buttonText3);
        assertAttributeEquals(buttonText3, "value", "Button Text");

        PageElement buttonText4 = elementFinder.find(By.id("button-text-test-4"));
        assertElementExists(buttonText4);
        assertAttributeEquals(buttonText3, "value", "Button Text");
    }

    @Test
    public void testButtonSubtle()
    {
        PageElement buttonSubtle1 = elementFinder.find(By.id("button-test-subtle"));
        assertElementExists(buttonSubtle1);
        assertTextEquals(buttonSubtle1, "Subtle Button");
        assertHasClass(buttonSubtle1, "aui-button-subtle");
    }

    @Test
    public void testButtonIcons()
    {
        // selector tests correct element
        PageElement buttonIcon0 = elementFinder.find(By.cssSelector("#button-icon-test-0 span"));
        assertElementExists(buttonIcon0);
        assertHasClass(buttonIcon0, "aui-icon");
        assertHasClass(buttonIcon0, "icon-extra-class");
        assertTextEquals(buttonIcon0, "Icon Text");

        PageElement buttonIcon1 = elementFinder.find(By.cssSelector("#button-icon-test-1 span"));
        assertElementExists(buttonIcon1);
        assertFalse("Should not have aui-icon class", buttonIcon1.hasClass("aui-icon"));
        assertHasClass(buttonIcon1, "icon-custom-class");

        PageElement buttonIcon2 = elementFinder.find(By.cssSelector("#button-icon-test-2 span"));
        assertElementExists(buttonIcon2);
        assertHasClass(buttonIcon2, "aui-icon");
        assertHasClass(buttonIcon2, "icon-extra-class");
        assertTextEquals(buttonIcon2, "Icon Text");

        // these are pretty weak tests - just ensures things still render when called with the ignored soy params
        PageElement buttonIcon3 = elementFinder.find(By.id("button-icon-test-3"));
        assertElementExists(buttonIcon3);
        assertFalse("Should not have aui-icon class", buttonIcon3.hasClass("aui-icon"));
        assertFalse("Should not have icon-extra-class class", buttonIcon3.hasClass("icon-extra-class"));

        PageElement buttonIcon4 = elementFinder.find(By.id("button-icon-test-4"));
        assertElementExists(buttonIcon4);
        assertFalse("Should not have aui-icon class", buttonIcon4.hasClass("aui-icon"));
        assertFalse("Should not have icon-extra-class class", buttonIcon4.hasClass("icon-extra-class"));
    }

    @Test
    public void testButtonSplit()
    {
        PageElement buttonSplit1 = elementFinder.find(By.id("soyTestButtonsSplit-button1"));
        assertHasRootElementProperties(buttonSplit1, "soyTestButtonsSplit-button1", "button");
        assertHasClass(buttonSplit1, "aui-button-split-main");

        PageElement buttonSplit2 = elementFinder.find(By.id("soyTestButtonsSplit-button2"));
        assertHasRootElementProperties(buttonSplit2, "soyTestButtonsSplit-button2", "button");
        assertHasClass(buttonSplit2, "aui-button-split-more");

        PageElement buttonSplit3 = elementFinder.find(By.id("soyTestButtonsSplit-button3"));
        assertHasRootElementProperties(buttonSplit3, "soyTestButtonsSplit-button3", "button");
        assertHasClass(buttonSplit3, "aui-button-primary");
        assertHasClass(buttonSplit3, "aui-button-split-main");

        PageElement buttonSplit4 = elementFinder.find(By.id("soyTestButtonsSplit-button4"));
        assertHasRootElementProperties(buttonSplit4, "soyTestButtonsSplit-button4", "button");
        assertHasClass(buttonSplit4, "aui-button-primary");
        assertHasClass(buttonSplit4, "aui-button-split-more");
    }

    public void assertAuiDropdownButton(String dropdownButtonId, String tagName, String ariaOwns)
    {
        PageElement auiDropdownButton = elementFinder.find(By.id(dropdownButtonId));
        assertAttributeEquals(auiDropdownButton, "aria-haspopup", "true"); // this is actually a string
        assertAttributeEquals(auiDropdownButton, "aria-owns", ariaOwns);
        assertHasClass(auiDropdownButton, "aui-button");
        assertHasClass(auiDropdownButton, "aui-dropdown2-trigger");
    }

    @Test
    public void testButtonDropdown2Integration()
    {
        assertAuiDropdownButton("button-dropdown2-test-1", "button", "dropdown");
        assertAuiDropdownButton("button-dropdown2-test-2", "a", "dropdown");
        assertAuiDropdownButton("button-dropdown2-test-3", "input", "dropdown");
        assertAuiDropdownButton("button-dropdown2-test-4", "input", "dropdown");
    }

    @Test
    public void testToolbar2()
    {
        PageElement toolbar2 = elementFinder.find(By.id("soy-toolbar2"));
        assertHasRootElementProperties(toolbar2, "soy-toolbar2", "div");
        assertAttributeEquals(toolbar2, "role", "toolbar");
        PageElement toolbar2inner = elementFinder.find(By.cssSelector("#soy-toolbar2 > .aui-toolbar2-inner"));
        assertElementExists(toolbar2inner);
    }

    @Test
    public void testToolbar2groups()
    {
        PageElement toolbar2group1 = elementFinder.find(By.cssSelector("#soy-toolbar2 > .aui-toolbar2-inner > #soy-toolbar2-group1"));
        assertHasRootElementProperties(toolbar2group1, "soy-toolbar2-group1", "div");

        PageElement toolbar2group2 = elementFinder.find(By.cssSelector("#soy-toolbar2 > .aui-toolbar2-inner > #soy-toolbar2-group2"));
        assertHasRootElementProperties(toolbar2group2, "soy-toolbar2-group2", "div");
    }

    @Test
    public void testToolbar2items()
    {
        PageElement toolbar2group1primary = elementFinder.find(By.cssSelector("#soy-toolbar2-group1 > .aui-toolbar2-primary"));
        assertHasRootElementProperties(toolbar2group1primary, "soy-toolbar2-group1-primary", "div");

        PageElement toolbar2group1secondary = elementFinder.find(By.cssSelector("#soy-toolbar2-group1 > .aui-toolbar2-secondary"));
        assertHasRootElementProperties(toolbar2group1secondary, "soy-toolbar2-group1-secondary", "div");

        PageElement toolbar2group2primary = elementFinder.find(By.cssSelector("#soy-toolbar2-group2 > .aui-toolbar2-primary"));
        assertHasRootElementProperties(toolbar2group2primary, "soy-toolbar2-group2-primary", "div");

        PageElement toolbar2group2secondary = elementFinder.find(By.cssSelector("#soy-toolbar2-group2 > .aui-toolbar2-secondary"));
        assertHasRootElementProperties(toolbar2group2secondary, "soy-toolbar2-group2-secondary", "div");
    }

    @Test
    public void testToolbar2ContentPlacement()
    {
        PageElement toolbar2Group1PrimaryButton = elementFinder.find(By.cssSelector("#soy-toolbar2-group1 > .aui-toolbar2-primary .aui-button"));
        assertHasRootElementProperties(toolbar2Group1PrimaryButton, "soy-toolbar2-primary-button", "button");

        PageElement toolbar2Group1SecondaryButton = elementFinder.find(By.cssSelector("#soy-toolbar2-group1 > .aui-toolbar2-secondary .aui-button"));
        assertHasRootElementProperties(toolbar2Group1SecondaryButton, "soy-toolbar2-secondary-button", "button");

        PageElement toolbar2Group2PrimaryButton = elementFinder.find(By.cssSelector("#soy-toolbar2-group2 > .aui-toolbar2-primary .aui-button"));
        assertHasRootElementProperties(toolbar2Group2PrimaryButton, "soy-toolbar2-primary-button2", "button");

        PageElement toolbar2Group2SecondaryButton = elementFinder.find(By.cssSelector("#soy-toolbar2-group2 > .aui-toolbar2-secondary .aui-button"));
        assertHasRootElementProperties(toolbar2Group2SecondaryButton, "soy-toolbar2-secondary-button2", "button");
    }

    @Test
    public void testBadge()
    {
        PageElement badge1 = elementFinder.find(By.id("soy-badge1"));
        assertHasRootElementProperties(badge1, "soy-badge1", "span");
        assertHasClass(badge1, "aui-badge");
        assertTextEquals(badge1, "9");
    }

    @Test
    public void testLabel()
    {
        PageElement label1 = elementFinder.find(By.id("soy-label1"));
        assertHasRootElementProperties(label1, "soy-label1", "span");
        assertHasClass(label1, "aui-label");
        label1.getText().contains("unclickable");

        PageElement label2 = elementFinder.find(By.id("soy-label2"));
        assertHasRootElementProperties(label2, "soy-label2", "a");
        assertHasClass(label2, "aui-label");
        assertFalse("Should not have aui-label-closeable class", label2.hasClass("aui-label-closeable"));
        label2.getText().contains("clickable");
        assertAttributeEquals(label2, "href", "http://example.com/");

        PageElement label3 = elementFinder.find(By.id("soy-label3"));
        assertHasRootElementProperties(label3, "soy-label3", "span");

        PageElement label3icon = elementFinder.find(By.cssSelector("#soy-label3 .aui-icon-close"));
        label3icon.getText().contains("(remove unclickableCloseable)");
        assertFalse("Icon should not have a title.", label3icon.getAttribute("title") == null);

        PageElement label4 = elementFinder.find(By.id("soy-label4"));
        assertHasRootElementProperties(label4, "soy-label4", "span");
        assertHasClass(label4, "aui-label");
        assertHasClass(label4, "aui-label-closeable");
        assertHasClass(label4, "aui-label-split");

        PageElement label4icon = elementFinder.find(By.cssSelector("#soy-label4 .aui-icon-close"));
        label4icon.getText().contains("CUSTOMTEXT");
        assertAttributeEquals(label4icon, "title", " CUSTOMTEXT");
    }

}
